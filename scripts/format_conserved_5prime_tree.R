args = commandArgs(TRUE)


#
# usage:
# R --vanilla --slave --args inFile(.phy) outFile(.pdf) < format_conserved_5prime_tree.R 
#

require(phytools)
require(ggtree)
library(RColorBrewer)

inFile <- as.character(args[1])
# inPath="${DATA}/06_trees/bootstrap-1000/conserved/"
# inFile <- paste0(inPath,"msa.nt.5prime.mafftl.phy")

outFile <- as.character(args[2])
# outPath="${DATA}/07_final_plots/"
# outFile <- paste0(outPath,"manuscriptFigure3.5prime.conserved.pdf")

min_bootstrap=50

flanking5prime <- read.newick(inFile)
flanking5prime$tip.label <- gsub("_5pr", "", flanking5prime$tip.label)

flanking5prime <- reroot(flanking5prime, 48)
flanking5prime$tip.label <- substr(flanking5prime$tip.label,1,3)

q <- ggtree(flanking5prime)
q <- rotate(q,42)
q <- rotate(q,61)

#add bst value to "the root"
q$data$label[q$data$label == ""] <- 0

d <- q$data
d <- d[!d$isTip,]
x <- is.na(as.numeric(d$label))
d$label[!x] <- as.numeric(d$label[!x])/10
d$label[x] <- 0
d <- d[as.numeric(d$label) > min_bootstrap,]

q <- q + geom_label(data=d, aes(label=paste(label,"%", sep=" ")))
q <- q + geom_treescale(0,40, width=0.1) 
q <- q + geom_tiplab(hjust=-.5)
q <- q + ggtitle("Phylogenetic tree of the 5 prime flanking sequences") 
q <- q + geom_cladelabel(node=62, label="KRTAP1-1", align=TRUE, angle=90, hjust='center', offset=.05)
q <- q + geom_cladelabel(node=43, label="KRTAP1-2", align=TRUE, angle=90, hjust='center', offset=.05)
q <- q + geom_cladelabel(node=71, label="KRTAP1-3", align=TRUE, angle=90, hjust='center', offset=.05)
q <- q + geom_cladelabel(node=52, label="KRTAP1-4", align=TRUE, angle=90, hjust='center', offset=.05)
q <- q + geom_hilight(node=62, fill=brewer.pal(4,"Dark2")[1], alpha=.5, extend=.05)
q <- q + geom_hilight(node=43, fill=brewer.pal(4,"Dark2")[2], alpha=.5, extend=.05)
q <- q + geom_hilight(node=71, fill=brewer.pal(4,"Dark2")[3], alpha=.5, extend=.05)
q <- q + geom_hilight(node=52, fill=brewer.pal(4,"Dark2")[4], alpha=.5, extend=.05)

pdf(file=outFile, 17, 12)
q
dev.off()

