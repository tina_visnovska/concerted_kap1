#!/usr/bin/perl

use strict;
use warnings;
use Bio::Seq;
use Bio::SeqIO;
#use Bio::Seq::Quality;
#use Bio::SeqIO::fastq;
use Getopt::Long;
use Pod::Usage;
use Data::Dumper;
#use File::Basename;

my $fastaFile;
my $help;

GetOptions(
	'help' => \$help,
	'fasta:s' => \$fastaFile
)or pod2usage(-msg=>"Invalid arguments", -verbose=>1);

pod2usage(-verbose=>2) if $help;


my $inFasta = Bio::SeqIO->new (-format => 'fasta', -file => $fastaFile);

while (my $seq = $inFasta->next_seq()){
	print STDOUT $seq->id, "\t", $seq->length, "\n" ; 
}

__END__

=head1 NAME

multiple_fasta_seq_names_and_lengths.pl - The script processes multiple fasta file
and for each sequence returns its ID and length. Meant to be used for whole genome 
assemblies and such.

=head1 SYNOPSIS 

perl multiple_fasta_seq_names_and_lengths.pl --help 

perl multiple_fasta_seq_names_and_lengths.pl --fasta <fastaFileName>
