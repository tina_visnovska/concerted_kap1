
This folder contains all the scripts used to process multiple 
genomic sequences from KAP1 gene family accross several species. 

The script `pipeline.sh` is the highest level script and gives 
detail description of the data processing. Given nature of some 
of the used tools (online web interface, interactive command-line 
tool), by only executing `pipeline.sh` one will not get whole 
the data analysis done at one step. However, all the data processing 
steps are documented in that file.

Dependencies on other tools and packages are documented in the file 
`dependencies.tsv`.
