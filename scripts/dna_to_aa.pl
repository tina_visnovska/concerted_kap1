#!/usr/bin/perl

use strict;
use warnings;
use Bio::Seq;
use Bio::SeqIO;

my $fileName = shift;

my $in = Bio::SeqIO->new( -file => $fileName ); 
my $nextSeq;
my $aaSeq;

while ( $nextSeq = $in->next_seq() ){
  $aaSeq = $nextSeq->translate;
  print ">",$aaSeq->id,"\n",$aaSeq->seq,"\n";
}
