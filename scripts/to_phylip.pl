#!/usr/bin/perl

use strict;
use warnings;
use Data::Dumper;
use Bio::Seq;
use Bio::SeqIO;

my $seq_count=shift;
my $msa_length=shift;
my $fileName = shift;
my $is_motif = shift;

my $in = Bio::SeqIO->new( -file => $fileName ); 
my $nextSeq;


print " $seq_count $msa_length\n";
while ( $nextSeq = $in->next_seq() ){
  if ($is_motif eq "YES" ){
    my $id = $nextSeq->id;
    $id =~ s/:subseq/_/g;
    $id =~ s/,/_/g;
    $id =~ s/\(/_/g;
    $id =~ s/\)/_/g;
    print $id, "\n";
  }else{ 
    my @name = split /[-,\.]/, $nextSeq->id;
    print substr($name[0],0,3)."1-".$name[2]."_".substr($name[3],0,3), "\n";
  }
  print $nextSeq->seq,"\n";
}
