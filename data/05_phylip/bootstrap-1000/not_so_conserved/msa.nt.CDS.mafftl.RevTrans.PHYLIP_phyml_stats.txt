
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
                                  ---  PhyML 20120412  ---                                             
                            http://www.atgc-montpellier.fr/phyml                                          
                         Copyright CNRS - Universite Montpellier II                                 
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			msa_nt_cds_mafftl_revtrans_fasta_gb_455bp_phylip
. Data set: 				#1
. Random init tree: 			#1
. Tree topology search : 		Best of NNIs and SPRs
. Initial tree: 			random tree
. Model of nucleotides substitution: 	HKY85
. Number of taxa: 			40
. Log-likelihood: 			-3997.06049
. Unconstrained likelihood: 		-2264.30374
. Parsimony: 				654
. Tree size: 				1.81103
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		0.629
. Transition/transversion ratio: 	4.276
. Nucleotides frequencies:
  - f(A)= 0.14646
  - f(C)= 0.36373
  - f(G)= 0.28588
  - f(T)= 0.20393

. Run ID:				none
. Random seed:				1470649286
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h0m23s (23 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			msa_nt_cds_mafftl_revtrans_fasta_gb_455bp_phylip
. Data set: 				#1
. Random init tree: 			#2
. Tree topology search : 		Best of NNIs and SPRs
. Initial tree: 			random tree
. Model of nucleotides substitution: 	HKY85
. Number of taxa: 			40
. Log-likelihood: 			-3997.06045
. Unconstrained likelihood: 		-2264.30374
. Parsimony: 				654
. Tree size: 				1.81269
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		0.629
. Transition/transversion ratio: 	4.275
. Nucleotides frequencies:
  - f(A)= 0.14646
  - f(C)= 0.36373
  - f(G)= 0.28588
  - f(T)= 0.20393

. Run ID:				none
. Random seed:				1470649286
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h0m18s (18 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			msa_nt_cds_mafftl_revtrans_fasta_gb_455bp_phylip
. Data set: 				#1
. Random init tree: 			#3
. Tree topology search : 		Best of NNIs and SPRs
. Initial tree: 			random tree
. Model of nucleotides substitution: 	HKY85
. Number of taxa: 			40
. Log-likelihood: 			-3996.04397
. Unconstrained likelihood: 		-2264.30374
. Parsimony: 				655
. Tree size: 				1.81839
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		0.628
. Transition/transversion ratio: 	4.303
. Nucleotides frequencies:
  - f(A)= 0.14646
  - f(C)= 0.36373
  - f(G)= 0.28588
  - f(T)= 0.20393

. Run ID:				none
. Random seed:				1470649286
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h0m18s (18 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			msa_nt_cds_mafftl_revtrans_fasta_gb_455bp_phylip
. Data set: 				#1
. Random init tree: 			#4
. Tree topology search : 		Best of NNIs and SPRs
. Initial tree: 			random tree
. Model of nucleotides substitution: 	HKY85
. Number of taxa: 			40
. Log-likelihood: 			-3996.03385
. Unconstrained likelihood: 		-2264.30374
. Parsimony: 				655
. Tree size: 				1.81876
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		0.628
. Transition/transversion ratio: 	4.308
. Nucleotides frequencies:
  - f(A)= 0.14646
  - f(C)= 0.36373
  - f(G)= 0.28588
  - f(T)= 0.20393

. Run ID:				none
. Random seed:				1470649286
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h0m18s (18 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			msa_nt_cds_mafftl_revtrans_fasta_gb_455bp_phylip
. Data set: 				#1
. Random init tree: 			#5
. Tree topology search : 		Best of NNIs and SPRs
. Initial tree: 			random tree
. Model of nucleotides substitution: 	HKY85
. Number of taxa: 			40
. Log-likelihood: 			-3997.01743
. Unconstrained likelihood: 		-2264.30374
. Parsimony: 				656
. Tree size: 				1.82455
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		0.626
. Transition/transversion ratio: 	4.286
. Nucleotides frequencies:
  - f(A)= 0.14646
  - f(C)= 0.36373
  - f(G)= 0.28588
  - f(T)= 0.20393

. Run ID:				none
. Random seed:				1470649286
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h0m21s (21 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

. Sequence filename: 			msa_nt_cds_mafftl_revtrans_fasta_gb_455bp_phylip
. Data set: 				#1
. Tree topology search : 		Best of NNIs and SPRs
. Initial tree: 			BioNJ
. Model of nucleotides substitution: 	HKY85
. Number of taxa: 			40
. Log-likelihood: 			-3997.01724
. Unconstrained likelihood: 		-2264.30374
. Parsimony: 				656
. Tree size: 				1.82460
. Discrete gamma model: 		Yes
  - Number of categories: 		4
  - Gamma shape parameter: 		0.626
. Transition/transversion ratio: 	4.287
. Nucleotides frequencies:
  - f(A)= 0.14646
  - f(C)= 0.36373
  - f(G)= 0.28588
  - f(T)= 0.20393

. Run ID:				none
. Random seed:				1470649286
. Subtree patterns aliasing:		no
. Version:				20120412
. Time used:				0h0m24s (24 seconds)

 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
 Suggested citations:
 S. Guindon, JF. Dufayard, V. Lefort, M. Anisimova, W. Hordijk, O. Gascuel
 "New algorithms and methods to estimate maximum-likelihood phylogenies: assessing the performance of PhyML 3.0."
 Systematic Biology. 2010. 59(3):307-321.

 S. Guindon & O. Gascuel
 "A simple, fast, and accurate algorithm to estimate large phylogenies by maximum likelihood"
 Systematic Biology. 2003. 52(5):696-704.
 oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
