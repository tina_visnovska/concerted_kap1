# Scripts and data to generate phylogenetic trees for:

**Contrasting patterns of coding and flanking region evolution in mammalian keratin associated protein-1 genes.** Zhou H, Visnovska T, Gong H, Schmeier S, Hickford J, Ganley ARD. [bioRxiv, doi:10.1101/282418](https://doi.org/10.1101/282418)

Data used in the manuscript are located in `data/07_final_plots`. 

When reading through scripts, start with `scripts/README.md` and `scripts/pipeline.sh`.
